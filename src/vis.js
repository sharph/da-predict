import Plot from 'react-plotly.js';

export function Histogram(props) {
  const layout = {width: 800, height: 200, showlegend: false,
                  margin: {l:30, r:10, t:10, b:20},
                  font: {family: "'Share Tech Mono', monospace"}};
  if (props.xaxis) {
    layout.xaxis = props.xaxis;
  }
  return (
    <Plot
      data={[
        {
          x: props.data,
          type: 'histogram',
          xbins: props.binning,
        }
      ]}
      layout={layout}
      config={{displayModeBar: false, staticPlot: true}}
    />
  );
}

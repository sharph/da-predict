import { compare, normalize, getCoeffs,
         estimateTurnout, montyCarlo } from './data';
import React, { Component } from 'react';
import { Histogram } from './vis';
import _ from 'lodash';

const trials = 1000;

const models = {
  '2017-general-krasner': {url: 'https://phillyelections.xyz/november_7__2017_general_election_district_attorney.json',
                           key: 'LAWRENCE S KRASNER'},
  '2017-primary-krasner': {url: 'https://phillyelections.xyz/may_16__2017_primary_election_district_attorney_dem.json',
                           key: 'LAWRENCE S KRASNER'},
  '2017-primary-khan': {url: 'https://phillyelections.xyz/may_16__2017_primary_election_district_attorney_dem.json',
                           key: 'JOE KHAN'},
  '2017-general-grossman': {url: 'https://phillyelections.xyz/november_7__2017_general_election_district_attorney.json',
                            key: 'BETH GROSSMAN'},
  '2020-primary-trump': {url: 'https://phillyelections.xyz/june_2__2020_primary_election_president_of_the_united_states_rep.json',
                         key: 'DONALD J TRUMP'},
  '2020-primary-biden': {url: 'https://phillyelections.xyz/june_2__2020_primary_election_president_of_the_united_states_dem.json',
                         key: 'JOSEPH R BIDEN'},
}
const electionResults = 'https://phillyelections.xyz/may_18__2021_primary_election_district_attorney_dem.json';
const _k = 'Larry Krasner';
const _K = 'LARRY KRASNER';
const _c = 'Carlos Vega';
const _C = 'CARLOS VEGA';

function percent(num) {
  return Math.round(num * 1000) / 10 + '%';
}

function comma(num) {
  return num.toLocaleString('en-US');
}

export default class App extends Component {
  constructor(props) {
    super();
    this.state = {
      data: {
      },
      results: null,
      updateIn: -1
    };
    this.delay = 15;
    Object.keys(models).map(k => {
      this.state.data[k] = null;
    });
    Object.keys(models).map(k => {
      fetch(models[k].url)
        .then(response => response.json())
        .then(data => {
          const newData = {...this.state.data};
          newData[k] = data.divs[models[k].key];
          this.setState({data: newData});
        });
    })
    this.loadResults();
  }

  componentDidMount() {
    this.updateTimer = setInterval(() => this.tick(), 1000);
  }

  componentWillUnmount() {
    clearInterval(this.updateTimer);
  }

  tick() {
    if (this.state.updateIn === 0) {
      this.loadResults();
    }
    this.setState({updateIn: this.state.updateIn - 1});
  }

  loadResults() {
    fetch(electionResults + '?' + (new Date()).getTime())
      .then(response => response.json())
      .then(data => {
        if (this.state.results && this.state.results.checked !== data.checked) {
          this.delay = 60;
        }
        this.setState({results: data})
      })
      .catch(() => { this.delay = 15; })
      .finally(() => this.setState({updateIn: this.delay}));
  }

  isReady() {
    let ready = true;
    Object.keys(models).map(k => {
      if (this.state.data[k] === null) {
        ready = false;
      }
    });
    if (this.state.results === null) {
      ready = false;
    }
    return ready;
  }

  render() {
    if (this.isReady()) {
      const votesK = this.state.results.results[_K];
      const votesC = this.state.results.results[_C];
      const votesW = this.state.results.results['Write-in'];
      const tot = votesK + votesC + votesW;
      return (
        <div className="App">
          <h1>2021 Philadelphia Democratic Primary</h1>
          <h2>Monty Carlo Simulator for District Attorney</h2>
          <h2>Current Real World Results</h2>
          <table style={{'padding-bottom': '2em'}}>
          <tr><td>Data checked:</td><td>{this.state.results.checked}</td></tr>
          <tr><td>Data changed:</td><td>{this.state.results.modified}</td></tr>
          <tr><td>Reloading in...</td><td>{this.state.updateIn}</td></tr>
          </table>
          <table>
            <tr><td>{_k}</td><td>{comma(votesK)}</td>
                <td>{percent(votesK / tot)}</td></tr>
            <tr><td>{_c}</td><td>{comma(votesC)}</td>
                <td>{percent(votesC / tot)}</td></tr>
            <tr><td>Write-in</td><td>{comma(votesW)}</td>
                <td>{percent(votesW / tot)}</td></tr>
          </table>
          <p>{this.state.results.reporting_divs}/{this.state.results.total_divs} divisions reporting</p>
          <p>Data is checked from <a href="https://results.philadelphiavotes.com/" target="_blank">results.philadelphiavotes.com</a> every
          2 minutes by the server, so when this page is notified of new data on the server it will adjust the reload timer to
          match the server. No need to mash reload.</p>
          <Analysis models={this.state.data} results={this.state.results.divs} changed={this.state.results.changed} />
        </div>
      );
    }
    return (<>
      <h2>Loading models...</h2>
      <div>
        {Object.keys(models).map((m) => (
          <div>
            {m} {this.state.data[m] ? 'ok!' : 'loading...'}
          </div>
        ))}
      </div>
      <h2>Loading realtime results...</h2>
      <div>
        2021 district attorney primary {this.state.results ? 'ok!' : 'loading...'}
      </div>
    </>);
  }
}

class Analysis extends Component {
  shouldComponentUpdate(nextProps) {
    return nextProps.changed !== this.props.changed;
  }

  render() {
    //const resK = _.pickBy(this.props.models['2017-general-krasner'],
    //  () => (Math.random() > 0.95));
    //const resC = _.pickBy(this.props.models['2017-general-grossman'],
    //  (v, k) => (k in resK));
    const resK = this.props.results[_K] || [];
    const resC = this.props.results[_C] || [];

    const k1 = montyCarlo(this.props.models['2017-general-krasner'], resK, trials / 2);
    const k2 = montyCarlo(this.props.models['2017-primary-krasner'], resK, trials / 2);
    const c1 = montyCarlo(this.props.models['2017-primary-khan'], resC, trials / 4);
    const c2 = montyCarlo(this.props.models['2017-general-grossman'], resC, trials / 4);
    const c3 = montyCarlo(this.props.models['2020-primary-trump'], resC, trials / 4);
    const c4 = montyCarlo(this.props.models['2020-primary-biden'], resC, trials / 4);

    const estK = k1.concat(k2);
    const estC = c1.concat(c2).concat(c3).concat(c4);
    estK.sort();
    estC.sort();
    const medianK = estK[Math.floor(estK.length / 2)];
    const medianC = estC[Math.floor(estC.length / 2)];
    estC.sort(() => (0.5 - Math.random()));
    const min = _.min([_.min(estK), _.min(estC)])
    const max = _.max([_.max(estK), _.max(estC)])
    const vs = _.map(_.zip(estK, estC), (r) => (100 * r[0] / (r[0] + r[1])));
    const Kwins = _.filter(vs, n => n >= 50).length;
    const Cwins = _.filter(vs, n => n < 50).length;
    return (
      <>
        <h2>Disclaimer</h2>
        <p>This is a toy and experiment. I hope it's interesting and produces a
           good guesstimate, but there's all sorts of things that could go wrong.</p>
        <h2>Guesstimated Results</h2>
            <p>{trials} total trials. {_k} wins in {percent(Kwins / trials)} of trials. {_c} wins
            in {percent(Cwins / trials)} of trials.</p>
            <table>
              <tr><td>{_k}</td><td>{comma(Math.round(medianK))}</td>
                  <td>{percent(medianK / (medianK + medianC))}</td></tr>
              <tr><td>{_c}</td><td>{comma(Math.round(medianC))}</td>
                  <td>{percent(medianC / (medianK + medianC))}</td></tr>
            </table>
        <h3>Percentage vote share</h3>
        <p>&gt; 50% favors Krasner</p>
        <Histogram data={vs}
          xaxis={{
            range: [0, 100]
          }}/>
        <h4>{_k}</h4>
        <Histogram data={estK} xaxis={{ range: [min, max] }}/>
        <h4>{_c}</h4>
        <Histogram data={estC} xaxis={{ range: [min, max] }}/>
        <h2>Info</h2>
        <p>
        This simulator uses current results from the Democratic District Attorney
        race in Philadelphia to simulate likely outcomes once all districts are
        reporting results. It uses sets of data from past elections as models to
        simulate the turnout for each candidate.
        </p>
        <p><a href="https://gitlab.com/sharph/da-predict">Source code!</a></p>
        <p><a href="https://twitter.com/phlsharp">@phlsharp</a></p>
        <h2>Model fits</h2>
        <p>A strong "peak" is an indication of a good model fit.</p>
        <h4>2017-general-krasner (for {_k})</h4>
          <Histogram data={getCoeffs(this.props.models['2017-general-krasner'], resK)} />
        <h4>2017-primary-krasner (for {_k})</h4>
          <Histogram data={getCoeffs(this.props.models['2017-primary-krasner'], resK)} />
        <h4>2017-primary-khan (for {_c})</h4>
          <Histogram data={getCoeffs(this.props.models['2017-primary-khan'], resC)} />
        <h4>2017-general-grossman (for {_c})</h4>
          <Histogram data={getCoeffs(this.props.models['2017-general-grossman'], resC)} />
        <h4>2020-primary-trump (for {_c})</h4>
          <Histogram data={getCoeffs(this.props.models['2020-primary-trump'], resC)} />
        <h4>2020-primary-biden (for {_c})</h4>
          <Histogram data={getCoeffs(this.props.models['2020-primary-biden'], resC)} />
      </>
    );
  }
}

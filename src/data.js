import _ from 'lodash';

export function getWeight(votes, totalvotes) {
  let sum;
  if (!totalvotes) {
    sum = _.chain(votes).values().sum().value();
  } else {
    sum = totalvotes;
  }
  return _.mapValues(votes, (v) => (v / sum));
}

export function normalize(votes) {
  const avg = _.chain(votes).values().sum().value() / _.values(votes).length;
  return _.mapValues(votes, (v) => (v / avg));
}

export function compare(a, b) {
  return _.chain(b)
    .pickBy((v, k) => (k in a))
    .mapValues((v, k) => {
      if (a[k] < 0) {
        return null;
      }
      return v / a[k];
    })
    .value();
}

export function getCoeffs(a, b) {
  const cmp = compare(a, b);
  const out = [];
  _.mapValues(cmp, (v, k) => {
    if (v === null) {
      return;
    }
    for (let n = 0; n < a[k]; n++) {
      out.push(v);
    }
  });
  return out;
}

export function comparePct(a, b) {
  // return b / a
  return _.chain(b)
    .pickBy((v, k) => (k in a))
    .mapValues((v, k) => ((v - a[k]) / a[k]))
    .value();
}

function pickRandom(L) {
  return L[Math.floor(Math.random()*L.length)];
}

export function estimateTurnout(model, known) {
  const modelTurnout = _.chain(model).values().sum().value();
  const knownTurnout = _.chain(known).values().sum().value();
  const modelTurnoutOfKnownDivisons = _.chain(model)
    .pickBy((v, k) => (k in known))
    .values().sum().value();
  return knownTurnout / (modelTurnoutOfKnownDivisons / modelTurnout);
}

export function montyCarlo(model, known, runs) {
  const coeffs = getCoeffs(model, known);
  // we have known / model, so it makes sense to multiply by a model value
  const results = [];
  for (let r = 0; r < runs; r++) {
    results.push(_.chain(model)
      .keys()
      .map((k) => {
        if (k in known) {
        // don't simulate what we know
          return known[k];
        }
        return model[k] * pickRandom(coeffs);
      })
      .sum().value());
  }
  return results;
}

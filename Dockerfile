FROM node:14 as BUILD

RUN mkdir /app

COPY . /app

WORKDIR /app

RUN npm install
RUN npm run build

FROM nginx:stable-alpine

RUN rm /usr/share/nginx/html/*
COPY --from=BUILD /app/build/ /usr/share/nginx/html/

EXPOSE 80
